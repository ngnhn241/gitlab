import java.awt.*;
import java.awt.event.*;
class Kahoot {
	public static void main(String[] args) {
		//========= Initial Menu =========
		Frame f = new Frame("KAHOOT");
		Button b1 = new Button("Game Moderator");
		Button b2 = new Button("Participant");
		Label l = new Label("KAHOOT!");
		Font titleFont = new Font(Font.MONOSPACED, Font.BOLD, 45);
		Font titleBut = new Font(Font.SANS_SERIF, Font.BOLD, 25);

		//========== Initial Game Moderator ==========
		Button b11 = new Button("Import Questions");
		Button b21 = new Button("Create Questions");
		Button b31 = new Button("Setting");
		Button b41 = new Button("Back");
		Button b51 = new Button("Done");

		//========= Menu =========
		l.setForeground(Color.WHITE);
		l.setFont(titleFont);
		l.setBounds(510, 150, 200, 50);
		l.setBackground(Color.BLUE);

		b1.setBounds(250, 350, 250, 100);
		b1.setBackground(Color.CYAN);
		b1.setForeground(Color.DARK_GRAY);
		b1.setFont(titleBut);
		b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
				f.removeAll();
				f.add(l);
				f.add(b11);
				f.add(b21);
				f.add(b31);
				f.add(b41);
				f.add(b51);
            }
        });

		b2.setBounds(700, 350, 250, 100);
		b2.setBackground(Color.CYAN);
		b2.setForeground(Color.DARK_GRAY);
		b2.setFont(titleBut);
		b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                f.setBackground(Color.ORANGE);
				l.setBackground(Color.ORANGE);
            }
        });

		f.add(l);
		f.add(b1);
		f.add(b2);
		f.setLayout(null);
		f.setSize(1200,700);
		f.setVisible(true);
		f.setBackground(Color.BLUE);
		f.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				System.exit(0);
			}
		});

		//========== Game Moderator ==========
		b11.setBounds(480, 250, 250, 70);
		b11.setBackground(Color.CYAN);
		b11.setForeground(Color.DARK_GRAY);
		b11.setFont(titleBut);

		b21.setBounds(480, 350, 250, 70);
		b21.setBackground(Color.CYAN);
		b21.setForeground(Color.DARK_GRAY);
		b21.setFont(titleBut);

		b31.setBounds(480, 450, 250, 70);
		b31.setBackground(Color.CYAN);
		b31.setForeground(Color.DARK_GRAY);
		b31.setFont(titleBut);

		b41.setBounds(200, 550, 150, 70);
		b41.setBackground(Color.CYAN);
		b41.setForeground(Color.DARK_GRAY);
		b41.setFont(titleBut);
		b41.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                f.removeAll();
				f.add(l);
				f.add(b1);
				f.add(b2);
            }
        });

		b51.setBounds(850, 550, 150, 70);
		b51.setBackground(Color.CYAN);
		b51.setForeground(Color.DARK_GRAY);
		b51.setFont(titleBut);
		b51.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                f.removeAll();
				f.add(l);
				f.add(b1);
				f.add(b2);
            }
        });
	}
}